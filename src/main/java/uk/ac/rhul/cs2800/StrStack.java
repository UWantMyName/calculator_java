package uk.ac.rhul.cs2800;

import java.util.EmptyStackException;

/**
 * Stack that holds String-type Entries.
 * 
 * @author Stefan Tanasa - 100939229
 *
 */
public class StrStack {
  private Stack strStack = new Stack();

  /**
   * Pushes a String value to the stack.
   * 
   * @param string The String that needs to be pushed to the stack
   */
  public void push(String string) {
    Entry entry = new Entry(string);
    strStack.push(entry);
  }
  
  /**
   * Returns the String of the top of the stack and removes it.
   * 
   * @return The top of the stack
   * @throws BadTypeException    If the type of the top Entry is erroneous
   * @throws EmptyStackException If the stack is empty
   */
  public String pop() throws BadTypeException {
    Entry top;

    try {
      top = strStack.pop();
    } catch (EmptyStackException e) {
      throw e;
    }
    return top.getString();
  }

  /**
   * Checks if the stack is empty or not.
   * 
   * @return True if the stack is empty, false otherwise
   */
  public boolean isEmpty() {
    return (this.strStack.size() == 0);
  }
}
