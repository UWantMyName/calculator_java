package uk.ac.rhul.cs2800;

/**
 * Main class for the stack that holds either numbers, symbols or strings.
 * 
 * @author Stefan Tanasa - 100939229
 *
 */
public class Entry {
  private float value;
  private Symbol symbol;
  private String str;
  private Type type;

  /**
   * Creates an entry with a specified float value.
   * 
   * @param val the float value
   */
  public Entry(float val) {
    this.value = val;
    this.symbol = Symbol.INVALID;
    this.str = null;
    this.type = Type.NUMBER;
  }

  /**
   * Creates an entry with a specified symbol.
   * 
   * @param sym the symbol
   */
  public Entry(Symbol sym) {
    this.symbol = sym;
    this.str = null;
    this.type = Type.SYMBOL;
  }

  /**
   * Creates an Entry with a given String.
   * 
   * @param s the string
   */
  public Entry(String s) {
    this.symbol = Symbol.INVALID;
    this.str = s;
    this.type = Type.STRING;
  }

  /**
   * Gets the value of the entry.
   * 
   * @return the value of the entry
   * @throws BadTypeException if the entry is not a number type
   */
  public float getValue() throws BadTypeException {
    if (this.type == Type.NUMBER) {
      return this.value;
    } else {
      throw new BadTypeException("The Entry does not have a valid Number attribute.");
    }
  }

  /**
   * Gets the symbol of the entry.
   * 
   * @return the symbol of the entry
   * @throws BadTypeException if the entry is not a symbol type
   */
  public Symbol getSymbol() throws BadTypeException {
    if (this.type == Type.SYMBOL) {
      return this.symbol;
    } else {
      throw new BadTypeException("The Entry does not have a valid Symbol attribute.");
    }
  }

  /**
   * Gets the String of the entry.
   * 
   * @return the String of the entry
   * @throws BadTypeException if the entry is not a string type
   */
  public String getString() throws BadTypeException {
    if (this.type == Type.STRING) {
      return this.str;
    } else {
      throw new BadTypeException("The Entry does not have a valid String attribute.");
    }
  }

  /**
   * Gets the type of the entry.
   * 
   * @return the type of the entry
   */
  public Type getType() {
    return this.type;
  }

  /**
   * Checks the equality between two objects.
   */
  @Override
  public boolean equals(Object o) {
    // self check
    if (this == o) {
      return true;
    }

    // null check
    if (o == null) {
      return false;
    }

    // check equality of classes
    if (this.getClass() != o.getClass()) {
      return false;
    }

    Entry en = (Entry) o;
    Type enType = en.getType();

    // if the types are the same, then I will check the equality of the respective
    // components
    if (this.type == enType) {
      if (this.type == Type.NUMBER) {
        float enValue = 0;

        try {
          enValue = en.getValue();
        } catch (BadTypeException e) {
          System.out.println(e.toString());
        }

        if (this.value == enValue) {
          return true;
        }
        return false;
      } else if (this.type == Type.STRING) {
        String enStr = "";

        try {
          enStr = en.getString();
        } catch (BadTypeException e) {
          System.out.println(e.toString());
        }

        if (this.str == enStr) {
          return true;
        }
        return false;
      } else if (this.type == Type.SYMBOL) {
        Symbol enSymbol = null;

        try {
          enSymbol = en.getSymbol();
        } catch (BadTypeException e) {
          System.out.println(e.toString());
        }

        if (this.symbol == enSymbol) {
          return true;
        }
        return false;
      } else {
        return true; // if both of them are invalid, then they are obviously equal
      }
    }

    return false;
  }

  /**
   * Returns the hashCode of the object.
   */
  @Override
  public int hashCode() {
    return (int) (this.value * (float) this.str.hashCode() * (float) this.symbol.hashCode()
        * (float) this.type.hashCode());
  }
}
