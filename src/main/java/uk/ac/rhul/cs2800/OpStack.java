package uk.ac.rhul.cs2800;

import java.util.EmptyStackException;

/**
 * Stack that holds Symbol-type Entries.
 * 
 * @author Stefan Tanasa - 100939229
 *
 */
public class OpStack {
  private Stack opStack = new Stack();

  /**
   * Pushes a Symbol value to the stack.
   * 
   * @param symbol The Symbol that needs to be pushed to the stack
   */
  public void push(Symbol symbol) {
    Entry e = new Entry(symbol);
    opStack.push(e);
  }

  /**
   * Returns the Symbol of the top of the stack and removes it.
   * 
   * @return The top of the stack
   * @throws BadTypeException    If the type of the top Entry is erroneous
   * @throws EmptyStackException If the stack is empty
   */
  public Symbol pop() throws BadTypeException {
    Entry top;

    try {
      top = opStack.pop();
    } catch (EmptyStackException e) {
      throw e;
    }
    return top.getSymbol();
  }

  /**
   * Checks if the stack is empty or not.
   * 
   * @return True if the stack is empty, false otherwise
   */
  public boolean isEmpty() {
    return (opStack.size() == 0);
  }
}
