package uk.ac.rhul.cs2800;

/**
 * Reverse Polish Calculator.
 * 
 * @author Stefan Tanasa 100939229
 *
 */
public class RevPolishCalc {
  private NumStack stack = new NumStack();

  /**
   * Checks if the string is a number or not.
   * 
   * @param str The string to check
   * @return If the string is a number or not
   */
  private boolean isNumber(final String str) {
    /*
     * If the first character of the string is a digit then clearly the string is a
     * number
     */
    char x = str.charAt(0);
    if ('0' <= x && x <= '9') {
      return true;
    }
    return false;
  }

  /**
   * Evaluates an expression given as a string.
   * 
   * @param expression The given expression
   * @return The result of the expression
   * @throws BadTypeException If the type of the top of the stack is erroneous.
   */
  public float evaluate(final String expression) throws BadTypeException {
    // Splitting the string into parts that can be either numbers or symbols.
    String[] parts = expression.split(" ");
    int i = 0; // index

    for (i = 0; i < parts.length; i++) {
      if (isNumber(parts[i])) {
        float number = Float.parseFloat(parts[i]);
        stack.push(number);
      } else {

        // I am extracting the first two numbers from the stack.
        // So I can later push the value of the operation between the two numbers.

        float firstNr = stack.pop();
        float secondNr = stack.pop();
        float result = 0;

        if (parts[i].equals("+")) {
          result = secondNr + firstNr;
        }
        if (parts[i].equals("-")) {
          result = secondNr - firstNr;
        }
        if (parts[i].equals("*")) {
          result = secondNr * firstNr;
        }
        if (parts[i].equals("/")) {
          result = secondNr / firstNr;
        }

        // Pushing the final result.
        stack.push(result);
      }
    }

    // the stack will only have one value, and that will be the result of the
    // expression
    return stack.pop();
  }
}
