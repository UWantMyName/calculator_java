package uk.ac.rhul.cs2800;

/**
 * Exception thrown if an Entry holds an erroneous type.
 * @author Stefan Tanasa - 100939229
 *
 */
public class BadTypeException extends Exception {
  /**
   * serialVersionUID.
   */
  private static final long serialVersionUID = -7851054470129137004L;

  // the message transfered through the constructor
  private String message;

  /**
   * Constructor that holds and resends the exception message.
   * @param mess the message the exception holds
   */
  public BadTypeException(String mess) {
    super(mess);
    this.message = mess;
  }

  @Override
  public String toString() {
    return "BadTypeException: " + this.message;
  }

}
