package uk.ac.rhul.cs2800;

/**
 * Infix notation calculator.
 * 
 * @author Stefan Tanasa - 100939229
 *
 */
public class InfixCalc {

  private OpStack opStack = new OpStack();
  private StrStack strStack = new StrStack();
  private StrStack strStack2 = new StrStack();
  private RevPolishCalc calc = new RevPolishCalc();

  /**
   * Checks if the string is a number or not.
   * 
   * @param str The string to check
   * @return If the string is a number or not
   */
  private boolean isNumber(final String str) {
    /*
     * If the first character of the string is a digit then clearly the string is a
     * number
     */
    char x = str.charAt(0);
    if ('0' <= x && x <= '9') {
      return true;
    }
    return false;
  }

  /**
   * Pushes the symbol to the stack (* or /).
   * 
   * @param sym The string to be pushed 
   */
  private void handleTimesDivide(String sym) {
    Symbol symbol = Symbol.INVALID;

    if (sym.equals("*")) {
      symbol = Symbol.TIMES;
    } else {
      symbol = Symbol.DIVIDE;
    }

    opStack.push(symbol);
  }

  /**
   * Pushes the symbol to the stack (+ or -).
   * 
   * @param sym The string to be pushed
   * @throws BadTypeException In case the Entry is of erroneous type
   */
  private void handlePlusMinus(String sym) throws BadTypeException {
    Symbol symbol = Symbol.INVALID;

    if (sym.equals("+")) {
      symbol = Symbol.PLUS;
    } else {
      symbol = Symbol.MINUS;
    }

    // Before I push the symbol to the stack, I will have to check for * or /.
    if (opStack.isEmpty()) {
      opStack.push(symbol);
    } else {
      Symbol sym2 = opStack.pop();

      if (sym2 == Symbol.DIVIDE) {
        strStack.push("/");
      } else if (sym2 == Symbol.TIMES) {
        strStack.push("*");
      } else {
        // I will push it back
        opStack.push(sym2);
      }

      opStack.push(symbol);
    }
  }
  
  /**
   * Pushes the symbol to the stack ( ')' ).
   * 
   * @throws BadTypeException In case the Entry is of erroneous type
   */
  private void handleRightBracket() throws BadTypeException {
    // I will have to take every single symbol in the opStack and push to the
    // StrStack
    Symbol sym = opStack.pop();

    while (sym != Symbol.LEFT_BRACKET) {
      if (sym == Symbol.PLUS) {
        strStack.push("+");
      } else if (sym == Symbol.MINUS) {
        strStack.push("-");
      } else if (sym == Symbol.TIMES) {
        strStack.push("*");
      } else if (sym == Symbol.DIVIDE) {
        strStack.push("/");
      }

      sym = opStack.pop();
    }
  }

  /**
   * Pushes the rest of the symbols to the stack.
   * 
   * @throws BadTypeException In case the Entry is of erroneous type
   */
  private void handleLeftoverOpStack() throws BadTypeException {
    Symbol sym = Symbol.INVALID;

    while (!opStack.isEmpty()) {
      sym = opStack.pop();

      if (sym == Symbol.PLUS) {
        strStack.push("+");
      } else if (sym == Symbol.MINUS) {
        strStack.push("-");
      } else if (sym == Symbol.TIMES) {
        strStack.push("*");
      } else if (sym == Symbol.DIVIDE) {
        strStack.push("/");
      }
    }
  }

  /**
   * Method that calculates the expression.
   * 
   * @param expr The given expression to be calculated
   */
  public float evaluate(String expr) throws BadTypeException {
    String[] parts = expr.split(" ");
    String expressionReversePolish = "";

    for (int i = 0; i < parts.length; i++) {
      if (isNumber(parts[i])) {
        strStack.push(parts[i]);
      } else if (parts[i].equals("*") || parts[i].equals("/")) {
        handleTimesDivide(parts[i]);
      } else if (parts[i].equals("+") || parts[i].equals("-")) {
        handlePlusMinus(parts[i]);
      } else if (parts[i].equals("(")) {
        opStack.push(Symbol.LEFT_BRACKET);
      } else if (parts[i].equals(")")) {
        handleRightBracket();
      }
    }

    handleLeftoverOpStack();

    // build the RevPolish expression
    while (!strStack.isEmpty()) {
      strStack2.push(strStack.pop());
    }

    // reverse the expression
    while (!strStack2.isEmpty()) {
      expressionReversePolish += strStack2.pop() + " ";
    }

    //System.out.println(expressionReversePolish);
    return calc.evaluate(expressionReversePolish);
  }

}
