package uk.ac.rhul.cs2800;

import java.util.EmptyStackException;

/**
 * Stack that holds numerical-type Entries.
 * 
 * @author Stefan Tanasa - 100939229
 *
 */
public class NumStack {
  private Stack numStack = new Stack();

  /**
   * Pushes a float value to the stack.
   * 
   * @param value The value that needs to be pushed to the stack
   */
  public void push(float value) {
    Entry entry = new Entry(value);
    numStack.push(entry);
  }

  /**
   * Returns the top of the stack and removes it.
   * 
   * @return The float value of the top of the stack
   * @throws BadTypeException    If the type of the top Entry is erroneous
   * @throws EmptyStackException If the stack is empty
   */
  public float pop() throws BadTypeException {
    Entry top;

    try {
      top = numStack.pop();
    } catch (EmptyStackException e) {
      throw e;
    }

    return top.getValue();
  }

  /**
   * Checks if the stack is empty or not.
   * 
   * @return True if the stack is empty, false otherwise
   */
  public boolean isEmpty() {
    return (numStack.size() == 0);
  }
}
