package uk.ac.rhul.cs2800;

/**
 * Enum that tells what an Entry holds.
 * @author Stefan Tanasa - 100939229
 *
 */
public enum Type {
  NUMBER("NUMBER"), SYMBOL("SYMBOL"), STRING("STRING"), INVALID("INVALID");

  // This is the field of the enum
  private String type;

  /**
   * Constructs a new enum based on the type given.
   * 
   * @param _type the type of the enum
   */
  private Type(String t) {
    this.type = t;
  }

  /**
   * Returns a string representation of the current Type enum.
   */
  @Override
  public String toString() {
    return this.type;
  }
}
