package uk.ac.rhul.cs2800;

/**
 * Symbol enum that holds the following symbols in an expression:
 * -, +, /, *, (, ) or INVALID.
 * @author Stefan Tanasa - 100939229
 *
 */
public enum Symbol {
  LEFT_BRACKET("LEFT BRACKET"), RIGHT_BRACKET("RIGHT BRACKET"), TIMES("TIMES"), DIVIDE("DIVIDE"),
  PLUS("PLUS"), MINUS("MINUS"), INVALID("INVALID");

  // This is the field of the enum.
  private String symbol;

  /**
   * Constructs a symbol based on the symbol given.
   * 
   * @param _symbol the symbol of the enum
   */
  private Symbol(String str) {
    this.symbol = str;
  }

  /**
   * Returns the string representation of the Symbol.
   */
  public String toString() {
    return this.symbol;
  }
}
