package uk.ac.rhul.cs2800;

import java.util.ArrayList;
import java.util.EmptyStackException;
import java.util.List;

/**
 * Stack class that holds entries.
 * @author Stefan Tanasa - 100939229
 *
 */
public class Stack {
  private int size;
  private List<Entry> entries;

  /**
   * Constructor that initializes the stack.
   */
  public Stack() {
    size = 0;
    entries = new ArrayList<Entry>();
  }

  /**
   * Pushes an entry to the stack.
   * 
   * @param entry the entry to put in the stack
   */
  public void push(Entry entry) {
    entries.add(size, entry);
    size++;
  }

  /**
   * Removes the top of the stack.
   * 
   * @return top of the stack
   */
  public Entry pop() throws EmptyStackException {
    if (size > 0) {
      size--;
      return entries.remove(size);
    } else {
      throw new EmptyStackException();
    }
  }

  /**
   * Gets the top of the stack.
   * 
   * @return top of the stack
   */
  public Entry top() throws EmptyStackException {
    if (size > 0) {
      return entries.get(size - 1);
    } else {
      throw new EmptyStackException();
    }
  }

  /**
   * Returns the size of the stack.
   * 
   * @return size of the stack
   */
  public int size() {
    return this.size;
  }
}
