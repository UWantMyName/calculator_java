
package uk.ac.rhul.cs2800;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test file that tests a few expressions.
 * 
 * @author Stefan Tanasa - 100939229
 *
 */
class TestRevPolishCalc {
  private RevPolishCalc calc = new RevPolishCalc();
  private String expr1 = "3 4 +";
  private String expr2 = "3 4 - 5 +";
  private String expr3 = "3 4 5 * -";
  private String expr4 = "3 4 - 5 *";
  private String expr5 = "9.3 4 + 9 - 5 * 4 3 + -";
  private String expr6 = "3 4 5 6 7 8 + + + + + 6 /";

  // Test 1
  @Test
  void test1() throws BadTypeException {
    assertEquals(calc.evaluate(expr1), 7.0f);
  }

  // Test 2
  @Test
  void test2() throws BadTypeException {
    assertEquals(calc.evaluate(expr2), 4.0f);
  }

  // Test 3
  @Test
  void test3() throws BadTypeException {
    assertEquals(calc.evaluate(expr3), -17.0f);
  }

  // Test 4
  @Test
  void test4() throws BadTypeException {
    assertEquals(calc.evaluate(expr4), -5.0f);
  }

  // Test 5
  @Test
  void test5() throws BadTypeException {
    assertEquals(calc.evaluate(expr5), 14.5f);
  }

  // Test 6
  @Test
  void test6() throws BadTypeException {
    assertEquals(calc.evaluate(expr6), 5.5f);
  }
}
