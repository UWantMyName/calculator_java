package uk.ac.rhul.cs2800;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test unit that evaluates infix expressions.
 * 
 * @author Stefan Tanasa - 100939229
 *
 */
class TestInfixCalc {

  private InfixCalc calc = new InfixCalc();
  private final String expr1 = "5 + ( 4 * 3 - 6 ) * 2";
  private final String expr2 = "( 1 + 1 ) * 13 + 10 / 2";
  private final String expr3 = "56 * 52 - ( 98 + 33 )";
  private final String expr4 = "( 25 + ( 14 * ( 32 - 10 * 3 ) + 2 ) - 4 )";
  private final String expr5 = "3 * ( ( 20 / 2 ) / 4 )";
  private final String expr6 = "20 + 9 * ( 20 / 5 - 1 )";

  @Test
  void test1() throws BadTypeException {
    assertEquals(calc.evaluate(expr1), 17.0);
  }

  @Test
  void test2() throws BadTypeException {
    assertEquals(calc.evaluate(expr2), 31.0);
  }

  @Test
  void test3() throws BadTypeException {
    assertEquals(calc.evaluate(expr3), 2781);
  }

  @Test
  void test4() throws BadTypeException {
    assertEquals(calc.evaluate(expr4), 51.0);
  }

  @Test
  void test5() throws BadTypeException {
    assertEquals(calc.evaluate(expr5), 7.5);
  }

  @Test
  void test6() throws BadTypeException {
    assertEquals(calc.evaluate(expr6), 47.0);
  }
}
