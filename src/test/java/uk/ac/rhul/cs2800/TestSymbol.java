package uk.ac.rhul.cs2800;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * 
 * @author Stefan Tanasa - 100939229
 *
 */
class TestSymbol {
  private Symbol symbolLB = Symbol.LEFT_BRACKET;
  private Symbol symbolRB = Symbol.RIGHT_BRACKET;
  private Symbol symbolPlus = Symbol.PLUS;
  private Symbol symbolMinus = Symbol.MINUS;
  private Symbol symbolTimes = Symbol.TIMES;
  private Symbol symbolDivide = Symbol.DIVIDE;
  private Symbol symbolInvalid = Symbol.INVALID;

  // Test 1
  // Test that checks the names for all the possible Symbols.
  @Test
  void testCheckNames() {
    assertEquals(symbolLB.toString(), "LEFT BRACKET");
    assertEquals(symbolRB.toString(), "RIGHT BRACKET");
    assertEquals(symbolPlus.toString(), "PLUS");
    assertEquals(symbolMinus.toString(), "MINUS");
    assertEquals(symbolTimes.toString(), "TIMES");
    assertEquals(symbolDivide.toString(), "DIVIDE");
    assertEquals(symbolInvalid.toString(), "INVALID");
  }
}
