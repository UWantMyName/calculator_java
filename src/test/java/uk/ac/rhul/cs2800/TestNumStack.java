package uk.ac.rhul.cs2800;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.EmptyStackException;
import org.junit.jupiter.api.Test;

/**
 * Test file that tests the main methods of the NumStack.
 * 
 * @author Stefan Tanasa 100939299
 *
 */
class TestNumStack {
  private NumStack numStack = new NumStack();
  private float entryValue = 5.0f;

  // Tests the push method of NumStack.
  @Test
  void testPush() {
    numStack.push(entryValue);
    assertFalse(numStack.isEmpty(), "The NumStack is not empty");
  }

  // Tests the pop method when the NumStack is not empty.
  @Test
  void testPop() throws BadTypeException {
    numStack.push(entryValue);
    numStack.pop();
    assertTrue(numStack.isEmpty(), "The NumStack is empty.");
  }

  // Tests the pop method when the NumStack is empty.
  @Test
  void testPopEmpty() throws EmptyStackException {
    assertThrows(EmptyStackException.class, () -> numStack.pop(), "Cannot pop an empty stack.");
  }
}
