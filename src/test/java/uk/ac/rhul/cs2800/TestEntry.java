package uk.ac.rhul.cs2800;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Test file that tests the main attributes and methods of entries.
 * @author Stefan Tanasa - 100939229
 *
 */
class TestEntry {
  private Entry entryValue = new Entry(5.0f);
  private Entry entryString = new Entry("cs2800");
  private Entry entrySymbol = new Entry(Symbol.DIVIDE);

  // Test 1
  /**
   * Test the types of all possible entries.
   */
  @Test
  void testTypes() {
    assertEquals(entryValue.getType(), Type.NUMBER,
        "Test that an entry with float value has a number type");
    assertEquals(entryString.getType(), Type.STRING,
        "Test that an entry with string has a string type");
    assertEquals(entrySymbol.getType(), Type.SYMBOL,
        "Test that an entry with symbol has a symbol type");
  }

  // Test 2
  /**
   * Test the BadTypeException for each entry.
   */
  @Test
  void testBadTypeException() {
    assertThrows(BadTypeException.class, () -> entryValue.getString(),
        "You can't get a string from an entry with value");
    assertThrows(BadTypeException.class, () -> entryString.getSymbol(),
        "You can't get a symbol from an entry with string");
    assertThrows(BadTypeException.class, () -> entrySymbol.getValue(),
        "You can't get a value from an entry with symbol");
  }

  // Test 3
  /**
   * Test the equality of two equal entries.
   */
  @Test
  void testEquality() {
    Entry e = new Entry(5.0f);
    assertTrue(entryValue.equals(e), "Both entries are equal");
  }

  // Test 4
  /**
   * Test the inequality of two different entries.
   */
  @Test
  void testInequality() {
    Entry e = new Entry(5.3f);
    assertFalse(entryValue.equals(e), "The entries are not equal");
  }

}
