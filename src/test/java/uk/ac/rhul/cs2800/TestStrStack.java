package uk.ac.rhul.cs2800;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.EmptyStackException;
import org.junit.jupiter.api.Test;

/**
 * Test file that tests the main methods of StrStack.
 * 
 * @author Stefan Tanasa - 100939229
 *
 */
class TestStrStack {
  private StrStack strStack = new StrStack();
  private String entryString = "(5 + 3) - 2";

  // Tests the push method of StrStack
  @Test
  void testPush() {
    strStack.push(entryString);
    assertFalse(strStack.isEmpty());
  }

  // Tests the pop method when the StrStack is not empty
  @Test
  void testPop() throws BadTypeException {
    strStack.push(entryString);
    strStack.pop();
    assertTrue(strStack.isEmpty());
  }

  //Tests the pop method when the OpStack is empty
  @Test
  void testPopEmpty() throws EmptyStackException {
    assertThrows(EmptyStackException.class, () -> strStack.pop(), "Cannot pop an empty stack.");
  }
}
