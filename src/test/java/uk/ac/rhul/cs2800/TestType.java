package uk.ac.rhul.cs2800;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/*
 * 
 * @author Stefan Tanasa - 100939229
 *
 */
class TestType {
  private Type typeNumber = Type.NUMBER;
  private Type typeString = Type.STRING;
  private Type typeSymbol = Type.SYMBOL;
  private Type typeInvalid = Type.INVALID;

  // Test 1
  // Test that checks the names of all the possible types.
  @Test
  void testCheckNames() {
    assertEquals(typeNumber.toString(), "NUMBER");
    assertEquals(typeString.toString(), "STRING");
    assertEquals(typeSymbol.toString(), "SYMBOL");
    assertEquals(typeInvalid.toString(), "INVALID");
  }
}
