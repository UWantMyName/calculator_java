package uk.ac.rhul.cs2800;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.EmptyStackException;
import org.junit.jupiter.api.Test;

/**
 * Test file that tests the main methods of the stack.
 * @author Stefan Tanasa - 100939229
 *
 */
public class TestStack {
  private Stack stack = new Stack();
  private Entry entrySymbol = new Entry(Symbol.DIVIDE);
  private Entry entryValue = new Entry(5.0f);
  private Entry entryString = new Entry("cs2800");

  /**
   * Pushes all the three entries.
   */
  private void pushAll() {
    stack.push(entrySymbol);
    stack.push(entryValue);
    stack.push(entryString);
  }

  // Test 1
  @Test
  void testPush() {
    pushAll();
  }

  // Test 2
  @Test
  void testSize() {
    pushAll();
    assertEquals(stack.size(), 3, "The stack has 3 elements");
  }

  // Test 3
  @Test
  void popNonEmptyStack() {
    pushAll();
    stack.pop();
    assertEquals(stack.size(), 2, "The stack has 2 elements");
    stack.pop();
    assertEquals(stack.size(), 1, "The stack has 1 element");
    stack.pop();
    assertEquals(stack.size(), 0, "The stack has 0 elements");
  }

  // Test 4
  @Test
  void popEmptyStack() {
    assertThrows(EmptyStackException.class, () -> stack.pop(), "Cannot pop an empty stack.");
  }

  // Test 5
  @Test
  void topNonEmpty() {
    pushAll();
    assertEquals(stack.pop().getType(), Type.STRING, "The top of the stack is a String Entry");
    assertEquals(stack.pop().getType(), Type.NUMBER, "The top of the stack is a Number Entry");
    assertEquals(stack.pop().getType(), Type.SYMBOL, "The top of the stack is a Symbol Entry");
  }

  // Test 6
  @Test
  void topEmpty() {
    assertThrows(EmptyStackException.class, () -> stack.top(),
        "Cannot get the top of an empty stack");
  }
}
