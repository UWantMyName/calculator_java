package uk.ac.rhul.cs2800;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.EmptyStackException;
import org.junit.jupiter.api.Test;

/**
 * Test file that tests the main methods of the OpStack.
 * 
 * @author Stefan Tanasa 100939229
 *
 */
class TestOpStack {
  private OpStack opStack = new OpStack();
  private Symbol entrySymbol = Symbol.DIVIDE;

  // Tests the push method of the OpStack
  @Test
  void testPush() {
    opStack.push(entrySymbol);
    assertFalse(opStack.isEmpty());
  }

  // Tests the pop method when the OpStack is not empty
  @Test
  void testPop() throws BadTypeException {
    opStack.push(entrySymbol);
    opStack.pop();
    assertTrue(opStack.isEmpty());
  }

  // Tests the pop method when the OpStack is empty
  @Test
  void testPopEmpty() throws EmptyStackException {
    assertThrows(EmptyStackException.class, () -> opStack.pop(), "Cannot pop an empty stack.");
  }
}
